import subprocess as sp
import argparse
import platform
from sys import platform as _platform
import os
import errno

# 1. Check OS 

print ("--------- OS Distribution ")

if _platform == "linux" or _platform == "linux2":
   # linux
   print "Running on a Linux platform"
   OsPlatform="Linux"
   OS=platform.linux_distribution()
   distribution=OS[0] 
   release=OS[1]
   print ('Distribution : ', distribution)
   print ('Release : ', release)

elif _platform == "darwin":
   # MAC OS X
   print "Running on a Mac platform"
   OsPlatform="Mac"
elif _platform == "win32":
   # Windows
   print "Running on a Windows32 platform"
   OsPlatform="Win32"
elif _platform == "win64":
    # Windows 64-bit
    print "Running on a Windows64 platform"
    OsPlatform="Win64"


# 2. Check Docker

print ('--------- Docker installation  ' )

child = sp.Popen("docker -v", shell=True, stdout=sp.PIPE)
streamdata = child.communicate()[0]
rc = child.returncode

if (int(rc)==0):
	print ('You already have docker installed .. ')
else:	
	print ("Docker installation compatible with your OS .. ")
	if(OsPlatform=='Linux'):
		
		if(distribution=='Ubuntu'):
			sp.call(["sudo","apt-get","update"])
			sp.call(["sudo", "apt-get","-y","install","docker-engine"])
		
		if(distribution=='CentOS' or distribution=='SLC'): # Centos / Scientific Linux
			# Set up the repository
			sp.call(["(sudo yum install -y yum -utils )"])  
			sp.call(["( sudo yum-config-manager \ --add-repo \ https://download.docker.com/linux/centos/docker-ce.repo )"])  
			sp.call(["( sudo yum-config-manager --enable docker-ce-edge )"])
			sp.call(["( sudo yum-config-manager --disable docker-ce-edge )"]) 
			# Install Docker
			sp.call(["(sudo yum makecache fast)"]) 
			sp.call(["(sudo yum install docker-ce)"])  # Install the latest version of Docker or specific version : sudo yum install docker-ce-<VERSION>

	if(OsPlatform=='Mac'):
		sp.call(["ruby","-e","$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"]) 
		sp.call(["( brew install docker-engine)"])

	#print ("--- Test if docker have been correctly installed  " )	
	#print (sp.call(["sudo","docker","run","hello-world"]) )

# 3. Docker Image

print ("--------- Docker image creation ")

BaseImg="FROM ubuntu:16.04"

# System packages 

update="RUN apt-get -qq update && apt-get -qq -y install curl && apt-get install bzip2"

# Install miniconda to /miniconda

i1="RUN curl -LO http://repo.continuum.io/miniconda/Miniconda-latest-Linux-x86_64.sh"
i2="RUN bash Miniconda-latest-Linux-x86_64.sh -p /miniconda -b"
i3="RUN rm Miniconda-latest-Linux-x86_64.sh"
i4="ENV PATH=/miniconda/bin:${PATH}"
i5="RUN conda update -y conda --yes"

Exau_list=["numpy","scipy","pandas","qtpy","seaborn","scikit-learn","spyder","sqlite","theano","tensorflow","h5py",
		   "keras","zlib","matplotlib","jupyter"]  # 14 packages

#Read user input

parser = argparse.ArgumentParser(description='Install ML packages')
parser.add_argument("-p", "--packages", default=[], type=str, nargs='+', help="List of packages to install")
parser.add_argument("-a", "--all", action='store_true', help="Install all ML packages available")
parser.add_argument("-c", "--custom", default=[], type=str, nargs='+', help="Custom packages to install, must be listed here https://repo.continuum.io/pkgs/free/linux-64/")

args = parser.parse_args()
pkgs= args.packages
cpkgs=args.custom
print ('List of available packages:' ,'%s' % ', '.join(map(str, Exau_list)))
print ("Also supported, any of Anaconda packages found in https://repo.continuum.io/pkgs/free/linux-64/")
# Install scientific dependencies.

cpt=0
dirc="eds"
if not os.path.exists(dirc):
    os.makedirs(dirc)

f = open(dirc+'/Dockerfile', 'w')
f.write(BaseImg+"\n")
f.write(update+"\n")
f.write(i1+"\n")
f.write(i2+"\n")
f.write(i3+"\n")
f.write(i4+"\n")
f.write(i5+"\n")

os.chdir(dirc)
if args.all:
	for p in Exau_list:
		f.write("RUN conda install "+p+" --yes "+" \n")
		cpt+=1

if args.packages :
	for p in pkgs:
		if p not in Exau_list:
			print ("****Did you mean:"+str([i for i in Exau_list if (i.startswith(p[:2]) or i.endswith(p[-3:]))][0])+"?")
		else:
			cpt+=1
			f.write("RUN conda install "+p+" --yes "+"\n")  
if args.custom:
	for c in cpkgs:
		cpt+=1
		f.write("RUN conda install "+c+" --yes "+"\n")

if "jupyter" not in pkgs:
	f.write("RUN conda install jupyter --yes \n") 
f.close()

if cpt>0:
	if sp.call(["docker", "build", "-t", "docker:img" ,"."])==0:
		print (sp.call(["docker", "run", "docker:img" ]))
		print (sp.call(["jupyter", "notebook"]))

print ('End------')
